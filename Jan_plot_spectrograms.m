% JAN_PLOT_SPECTROGRAMS
% 一括して1枚の画像としてスペクトログラムを表示するためのスクリプト

% Prepare
x_len = 0.5;   % 元の音声の信号長（秒）

subplot(2, 2, 1);
  plot_spectrogram(Jan_Goo1_PowX_dB, x_len, Fs);
  title('Chk 入力1');
  
subplot(2, 2, 2);
  plot_spectrogram(Jan_Chk1_PowX_dB, x_len, Fs);
  title('Chk 入力2');

subplot(2, 2, 3);
  plot_spectrogram(Jan_Chk2_PowX_dB, x_len, Fs);
  title('Chk 参照');

subplot(2, 2, 4);
  plot_spectrogram(Jan_Par2_PowX_dB, x_len, Fs);
  title('Par 参照');


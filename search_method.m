initial_node = 1;
final_node = mn2nodeid(end);

open_list = [ struct('id', initial_node, 'parent', 0, 'cost', 0) ];
closed_list = [];

 
 while ~isempty(open_list)
 
 cur_node = open_list(1);
 open_list(1) = [];
 closed_list = [closed_list cur_node];
 [[closed_list.id]; [closed_list.parent]; [closed_list.cost]];
 
 if (cur_node.id == final_node)
     disp('I got Final node!')
    break
 end
 
 node_ids = find(A([cur_node.id], :) > 0);
 
 if ~isempty(closed_list)
   node_ids([find(ismember(node_ids, [closed_list.id]) == 1)]) = [];
 end
 
 node_costs = A(cur_node.id, node_ids) + cur_node.cost;
 node_parents = repmat(cur_node.id, size(node_ids));
 hyp_nodes = struct('id', num2cell(node_ids),...
                    'parent', num2cell(node_parents),... 
                    'cost', num2cell(node_costs));
 
 open_list = [ open_list hyp_nodes];
 [~, idx] = sort([open_list.cost]);
 open_list = open_list(idx);
 [~, idx] = unique([open_list.id], 'stable');
 open_list = open_list(idx);
 
 end
 
 cur_id = final_node;
 route = [];
 while cur_id > 0
    route = [cur_id route];
    array_index = find([closed_list.id] == cur_id);
    cur_id = closed_list(array_index(1)).parent;
 end
 %route;
 distance = cur_node.cost
 %route_method;
%% Plot the Graph
G = digraph(A);
p = plot(G);
%p = plot(G, 'EdgeLabel', round(G.Edges.Weight));

% ノードのXY座標を指定
[nodes_Y, nodes_X] = ind2sub(size(mn2nodeid), 1:mn2nodeid(end));
p.XData = nodes_X; % x座標
p.YData = nodes_Y; % y座標

% 星形 'p', 〇 'o'
highlight(p, initial_node, 'Marker', 'p', 'MarkerSize', 10);
highlight(p, final_node, 'Marker', 'o', 'MarkerSize', 10);

% 経路上のエッジとノードを赤（r）で目立たせる
highlight(p, route, 'EdgeColor','r', 'NodeColor', 'r');

% 以下は使うデータによって適宜変える必要がある
xlabel('n (Frame number of Input speech)');
ylabel('m (Frame number of Reference speech)');
xlim([0 11]); ylim([0 11]);
title(['Input: Chk2; ' 'Reference: Par; ' 'Total cost = ' num2str(distance)]);


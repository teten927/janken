clear;
rec = audiorecorder(16000, 16, 1);  % 16000 Hz, 16 bit, 1 channel
frame_len = 800;
fft_len = 1024;
result_string_table = {'Goo', 'Chk', 'Par'};

Jan_load_wavefiles;

disp('3'); pause(1); disp('2'); pause(1); disp('1'); pause(1); disp('Go!'); % count down
recordblocking(rec, 0.5);
x = getaudiodata(rec);

frame_x_in = signal2frame(x, frame_len, fft_len);
PowX_dB_in = calc_powerspec(frame_x_in, fft_len);

frame_x_ref = signal2frame(Jan_Goo(:, 1), frame_len, fft_len);
PowX_dB_ref = calc_powerspec(frame_x_ref, fft_len);
Adjacency_Matrix;
search_method;
Dist(1) = distance;

frame_x_ref = signal2frame(Jan_Goo(:, 1), frame_len, fft_len);
PowX_dB_ref = calc_powerspec(frame_x_ref, fft_len);
Adjacency_Matrix;
search_method;
if distance < Dist(1)
    Dist(1)= distance;
end

frame_x_ref = signal2frame(Jan_Chk(:, 1), frame_len, fft_len);
PowX_dB_ref = calc_powerspec(frame_x_ref, fft_len);
Adjacency_Matrix;
search_method;
Dist(2) = distance;

frame_x_ref = signal2frame(Jan_Chk(:, 1), frame_len, fft_len);
PowX_dB_ref = calc_powerspec(frame_x_ref, fft_len);
Adjacency_Matrix;
search_method;
if distance < Dist(2)
    Dist(2)= distance;
end

frame_x_ref = signal2frame(Jan_Par(:, 1), frame_len, fft_len);
PowX_dB_ref = calc_powerspec(frame_x_ref, fft_len);
Adjacency_Matrix;
search_method;
Dist(3) = distance;
disp(Dist)

frame_x_ref = signal2frame(Jan_Par(:, 1), frame_len, fft_len);
PowX_dB_ref = calc_powerspec(frame_x_ref, fft_len);
Adjacency_Matrix;
search_method;
if distance < Dist(3)
    Dist(3)= distance;
end

[~, idx] = min(Dist);

disp(result_string_table{idx});
%% Lookup table: Maze axis (m, n) -> Node Index (node.id)
% cf. IElem or DElem macros in Image Processing Experiment
[~, n_max] = size(PowX_dB_in);
[~, m_max] = size(PowX_dB_ref);
mn2nodeid = reshape(1:(n_max*m_max), [m_max n_max]);

%% Prepare Adjacency Matrix
A = zeros(n_max*m_max);
for n = 2:n_max
    for m = 1:m_max
        % Calculate distance in frame m in reference speech and frame n in input speech

        d_mn = sqrt( sum( (PowX_dB_ref(:, m) - PowX_dB_in(:, n)).^2 ) );

        dst_nodeid = mn2nodeid(m, n);

        A(mn2nodeid(  m, n-1), dst_nodeid) = d_mn;

        if m >= 2
            A(mn2nodeid(m-1, n-1), dst_nodeid) = d_mn;
        end

        if m >= 3
            A(mn2nodeid(m-2, n-1), dst_nodeid) = d_mn;
        end
  end
end